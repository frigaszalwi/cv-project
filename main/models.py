from django.db import models
from cloudinary.models import CloudinaryField
# Create your models here.

class Project(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    date_published = models.DateField()
    image = CloudinaryField('image')
    link = models.TextField()

    def __str__(self):
        return self.title 

class Blog(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    date_published = models.DateField()

    def __str__(self):
        return self.title 
