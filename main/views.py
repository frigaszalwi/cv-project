from urllib import response
from django.shortcuts import render
from .models import Project, Blog

# Create your views here.
def index(request):
    return render(request, 'main.html', {})

def project(request):
    project_all = Project.objects.all()
    response = {'project' : project_all}
    return render(request, 'project.html', response)

def detail_project(request, id):
    project = Project.objects.all().get(id=id)
    response = {'project' : project}
    return render(request, "detail_project.html", response)

def blog(request):
    blog_all = Blog.objects.all()
    response = {'blog' : blog_all}
    return render(request, 'blog.html', response)

def detail_blog(request, id):
    blog = Blog.objects.all().get(id=id)
    response = {'blog' : blog}
    return render(request, "detail_blog.html", response)